<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;



$router->group(['prefix'=>'api/'], function() use ($router){
    $router->post('/produtos', ['uses' => 'Produtos\ProdutoController@create']);        // criar
    $router->get('/produtos', ['uses' => 'Produtos\ProdutoController@getAll']);        // Listar Todos
    $router->get('/produtos/{id}', ['uses' => 'Produtos\ProdutoController@get']);      // Listar 1 unico registro
    $router->put('/produtos/{id}', ['uses' => 'Produtos\ProdutoController@update']);    // Atualizar um registro
    $router->delete('/produtos/{id}', ['uses' => 'Produtos\ProdutoController@delete']); // deletar um registro

});


$router->get('/', function () use ($router) {
    return $router->app->version();
});

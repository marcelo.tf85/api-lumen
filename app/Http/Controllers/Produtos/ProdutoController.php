<?php
declare(strict_types=1);


namespace App\Http\Controllers\Produtos;


use App\Http\Controllers\Controller;
use App\Services\Produtos\ProdutoService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ProdutoController
 * @package App\Http\Controllers\Produtos
 */

class ProdutoController extends Controller
{
    private $ProdutoService;

    /**
     * ProdutoController constructor.
     * @param ProdutoService $ProdutoService
     */
    public function __construct(ProdutoService $ProdutoService)
    {
        $this->ProdutoService = $ProdutoService;
    }

    /**
     * @param $limit
     * @param $orderBy
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        return $this->ProdutoService->getAll($limit = 2);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function get($id): JsonResponse
    {
      return $this->ProdutoService->get($id);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
       return $this->ProdutoService->create($request);
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update($id, Request $request): JsonResponse
    {
      return $this->ProdutoService->update($id, $request);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
      return $this->ProdutoService->delete($id);
    }
}

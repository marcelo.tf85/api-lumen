<?php
declare(strict_types=1);


namespace App\Services\Produtos;


use App\Models\Produtos\ValidationsProdutos;
use App\Repositories\Produtos\RepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Class ProdutoService
 * @package App\Services\Produtos
 */

class ProdutoService
{
    /**
     * @var RepositoryInterface
     */
    private $ProdutoRepository;


    public function __construct( RepositoryInterface $ProdutoRepository)
    {
        $this->ProdutoRepository = $ProdutoRepository;
    }


    public function getAll($limit): JsonResponse
    {
        try {
            $result =  $this->ProdutoRepository->getAll($limit);
            if ($result > '0'){
                return response()->json($result, Response::HTTP_OK);
            }else{
                return response()->json(null, Response::HTTP_OK);
            }
        }catch (QueryException $err){
            return response()->json(['Erro' => 'Erro de conexão com o Banco de dados', Response::HTTP_INTERNAL_SERVER_ERROR]);
        }

    }

    public function get($id): JsonResponse
    {
        try {
             $result = $this->ProdutoRepository->get($id);
            if ($result  > '0'){
                return response()->json($result, Response::HTTP_OK);
            }else{
                return response()->json(null, Response::HTTP_OK);
            }
        }catch (QueryException $err){
            return response()->json(['Erro' => 'Erro de conexão com o Banco de dados', Response::HTTP_INTERNAL_SERVER_ERROR]);
        }

    }

    public function create(Request $request): JsonResponse
    {
        $validar = validator::make(
            $request->all(),
            ValidationsProdutos::Roles
        );
        if ($validar->fails()){
            return response()->json($validar->errors(), Response::HTTP_BAD_REQUEST);
        }else{
            try {
                $result = $this->ProdutoRepository->create($request);
                return response()->json($result, Response::HTTP_CREATED);
            }catch (QueryException $err){
                return response()->json(['Erro' => 'Erro de conexão com o Banco de dados', Response::HTTP_INTERNAL_SERVER_ERROR]);
            }
        }
    }

    public function update($id, Request $request): JsonResponse
    {
        $validar = validator::make(
            $request->all(),
            ValidationsProdutos::Roles
        );
        if ($validar->fails()){
            return response()->json($validar->errors(), Response::HTTP_BAD_REQUEST);
        }else{
            try {
                $result = $this->ProdutoRepository->update($id, $request);
                return response()->json($result, Response::HTTP_ACCEPTED);
            }catch (QueryException $err){
                return response()->json(['Erro' => 'Erro de conexão com o Banco de dados', Response::HTTP_INTERNAL_SERVER_ERROR]);
            }
        }

    }

    public function delete($id): JsonResponse
    {
        try {
            $this->ProdutoRepository->delete($id);
            return response()->json(null, Response::HTTP_NO_CONTENT);
        }catch (QueryException $err){
            return response()->json(['Erro' => 'Erro de conexão com o Banco de dados', Response::HTTP_INTERNAL_SERVER_ERROR]);
        }

    }
}

<?php
declare(strict_types=1);


namespace App\Models\Produtos;


/**
 * Class ValidationsProdutos
 * @package App\Models\Produtos
 */
class ValidationsProdutos
{
    const Roles =
        [
            'descricao' => 'required | min:3 max:100',
            'datavalidade' => 'required',
            'valor' => 'required'
        ];

}

<?php

declare(strict_types=1);


namespace App\Models\Produtos;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Produto
 * @package App\Models\Produtos
 */
class Produto extends Model
{
    /**
     * @var string
     */
    protected $table = 'produtos';


    /**
     * @var string[]
     */
    protected $fillable = [
        'descricao',
        'datavalidade',
        'lote',
        'idcategoria',
        'valor'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;


}

<?php
declare(strict_types=1);


namespace App\Repositories\Produtos;


use App\Models\Produtos\Produto;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;



/**
 * Class ProdutoRepository
 * @package App\Repositories
 */
class ProdutoRepository implements RepositoryInterface
{
    /**
     * @var Produto
     */
    private $model;

    /**
     * ProdutoController constructor.
     * @param Produto $produto
     */
    public function __construct(Produto $produto)
    {
        $this->model = $produto;
    }

    /**
     * @param int $limit
     * @param $orderBy
     * @return Produto[]|\Illuminate\Contracts\Pagination\LengthAwarePaginator|Collection|JsonResponse
     */
    public function getAll($limit = 5)
    {
        $result = $this->model::query();

        return $result->paginate($limit)
            ->appends([
                'limit'=> $limit
            ]);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function get($id)
    {
       return $this->model->find($id);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
         return $this->model->create($request->all());
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update($id, Request $request)
    {
           return $this->model->find($id)-> update($request ->all());
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id)
    {
       return $this->model->find($id)-> delete();
    }
}

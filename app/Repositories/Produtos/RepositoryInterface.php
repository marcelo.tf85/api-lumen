<?php
declare(strict_types=1);

namespace App\Repositories\Produtos;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Interface RepositoryInterface
 * @package App\Repositories
 */
interface RepositoryInterface
{
    public function getAll($limit);

    public function get($id);

    public function create(Request $request);

    public function update($id, Request $request);

    public function delete($id);

}
